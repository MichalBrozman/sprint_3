package com.example.hp.sprint_3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.File;
import  android.widget.*;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    private TextView textView;
    private EditText editText;
    private Button saveButton;
    private Button applybutton;

    public static final String SHARED_PREFS = "sharedPreferences";
    public static final String TEXT = "text";

    private  String text;


    private Button writeText, readText;
    private EditText enterText;
    private TextView showText;
    private String file = "myfile";
    private String fileContents;


    DatabaseHelper mDatabaseHelper;
    private Button btnAdd, btnViewData;
    private EditText editTextTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView=(TextView) findViewById(R.id.textview);
        editText = (EditText) findViewById(R.id.edittext);
        applybutton = (Button) findViewById(R.id.apply_button);
        saveButton = (Button) findViewById(R.id.save_button);


        writeText = findViewById(R.id.writeText);
        readText = findViewById(R.id.readText);
        enterText = findViewById(R.id.enterText);
        showText = findViewById(R.id.showText);


        editTextTwo = (EditText) findViewById(R.id.editTextTwo);
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnViewData = (Button) findViewById(R.id.btnView);
        mDatabaseHelper = new DatabaseHelper(this);


        applybutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(editText.getText().toString());
            }

        });
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
            }
        });
        loadData();
        updateViews();


        //File
        writeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fileContents = enterText.getText().toString();
                try{
                    FileOutputStream fOut = openFileOutput(file, MODE_PRIVATE);
                    fOut.write(fileContents.getBytes());
                    fOut.close();
                    File fileDir = new File(getFilesDir(), file);
                    Toast.makeText(getBaseContext(), "File Saved at" +fileDir, Toast.LENGTH_LONG).show();
                    }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        readText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                  FileInputStream fIn =  openFileInput(file);
                  int c;
                  String temp ="";

                  while ((c = fIn.read())!= -1){
                      temp = temp + Character.toString((char)c);

                  }
                  showText.setText(temp);
                }
            catch (Exception e){
                    e.printStackTrace();
            }
            }

        });

//SQL
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newEntry = editTextTwo.getText().toString();
                if (editTextTwo.length() != 0) {
                    AddData(newEntry);
                    editTextTwo.setText("");
                } else {
                    toastMessage("You must put something in the text field!");
                }

            }
        });

        btnViewData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListDataActivity.class);
                startActivity(intent);
            }
        });


    }

    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor= sharedPreferences.edit();

        editor.putString(TEXT, textView.getText().toString());

        editor.apply();

        Toast.makeText(this, "Data saved", Toast.LENGTH_SHORT).show();
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        text = sharedPreferences.getString(TEXT, "");

    }
    public void updateViews(){
        textView.setText(text);

    }
    public void AddData(String newEntry) {
        boolean insertData = mDatabaseHelper.addData(newEntry);

        if (insertData) {
            toastMessage("Data Successfully Inserted!");
        } else {
            toastMessage("Something went wrong");
        }
    }


    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

}
